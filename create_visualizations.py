import pandas as pd
import numpy as np
import pdb
import matplotlib.pyplot as plt
import mplcursors
import requests
from bs4 import BeautifulSoup
import statsmodels.api as sm
import re
import json


def age_line_chart():
    """
    Create a visualization of the different infection rates per day.
        Parameters: None.
        Returns: None.
        Side-Effects: Creates a pyplot line chart of the different infection
            types.
        Dependencies: Depends on TimeAge.csv file to supply data.
    """
    # Read in original CSV, convert datetime objects.
    with open("covid_sk_dataset/TimeAge.csv", 'r') as file:
        df = pd.read_csv(file)
    df = df[["date", "age", "confirmed"]]
    df["date"] = pd.to_datetime(df["date"], format="%Y-%m-%d")

    # Create new dataframe by processing original dataframe.
    age_df = pd.DataFrame(columns=df.age.unique(), index=df.date.unique())
    for _, row in df.iterrows():
        age_df[row["age"]][row["date"]] = row["confirmed"]

    # Draw plot.
    age_df.plot()
    plt.ylabel("Number of reported cases", fontsize=16)
    plt.xlabel("Date", fontsize=16)

    # Create cursor.
    def gen_hover_label(x):
        x.annotation.set_text(f"Cases: {int(round(x.target[1]))}")
    cursor = mplcursors.cursor(plt.gcf(), hover=True)
    cursor.connect("add", gen_hover_label)


def infection_location_scatterplot():
    """
    Create a scatterplot of the different infection rates, with x = population
    (measured as sum(k12 + university + elderly)) and y = infection numbers.
    Parameters: None.
    Returns: None.
    Side-Effects: Creates a pyplot scatterplot of the different infection rates
        by city.
    """
    # Read in original CSV, convert datetime objects.
    with open("covid_sk_dataset/Region.csv", 'r') as file:
        df = pd.read_csv(file)
    df["total_infected"] = df["elementary_school_count"] + df[
        "kindergarten_count"] + df["university_count"] + df[
            "nursing_home_count"]
    df = df[["province", "city", "total_infected"]]

    url = "https://worldpopulationreview.com/countries/cities/south-korea"

    response = requests.get(url)
    soup = BeautifulSoup(response.content, "lxml")
    data = str(soup.find("script", {"id": "__NEXT_DATA__"}))
    objects = re.findall(r'{[^{}]*}', data)
    objects = [json.loads(obj) for obj in objects]
    city_populations = dict()
    for obj in objects:
        try:
            name = obj["name"]
            population = obj["pop"]
        except KeyError:
            continue
        else:
            city_populations[name] = population

    def grab_city_population(row):
        try:
            return city_populations[row["city"]]
        except KeyError:
            return np.NaN
    df["population"] = df.apply(grab_city_population, axis=1)

    # Remove cities where population could not be found, encode color.
    df.dropna(inplace=True)
    unique_provinces = list(df.province.unique())
    df["color"] = [unique_provinces.index(x) for x in df.province]
    df.index = range(len(df))

    # Create scatterplot.
    plt.scatter(df["population"], df["total_infected"], c=df.color, s=30)

    # Create best-fit line.
    ols = _get_ols_parameters(df["population"], df["total_infected"])
    y = ols[0] * df["population"] + ols[1]
    plt.plot(df["population"], y)

    # Create labels.
    plt.ylabel("Total Infections", fontsize=16)
    plt.xlabel("Population", fontsize=16)


def city_geochart(df):
    """
    Creates a chart overlayed on a map of south asia, showing the different
    infection rates in various regions.
    Side-Effects: Creates a pyplot geoplot of the different infection rates by
        city.
    """
    pass


def _get_ols_parameters(seriesX, seriesY):
    """
    Take in a series, fit a line to it, and return it's OLS parameters.
    Parameters:
        series (pd.Series): Series of floats to construct line from.
    Returns:
        (list[float]): List containing the slope, intercept, R-squared, and
            p-value of line.
    """
    # Create the OLS object.
    pdb.set_trace()
    X = sm.add_constant(seriesX.values)
    model = sm.OLS(seriesY, X)    # Fit the model, and return the values.
    results = model.fit()
    return [
        results.params["x1"],
        results.params["const"],
        results.rsquared,
        results.pvalues["x1"]
    ]


def main():
    age_line_chart()
    plt.figure()
    infection_location_scatterplot()
    plt.show()


if __name__ == "__main__":
    main()
